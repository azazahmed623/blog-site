<?php

use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Frontend\FrontendController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontendController::class, 'index'])->name('frontend.index');

Route::get('/categories/{category}/post', [FrontendController::class, 'postList'])->name('frontend.posts.index');
Route::get('/posts/{post}', [FrontendController::class, 'postDetails'])->name('frontend.posts.show');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->prefix('/dashboard')->group(function () {
    Route::get('/', [BackendController::class, 'index'])->name('backend.index');

    Route::post('/posts/{post}/comments', [CommentController::class, 'store'])->name('posts.comments.store');

    Route::get('/categories-trash', [CategoryController::class, 'trash'])->name('categories.trash');
    Route::get('/categories/{id}/restore', [CategoryController::class, 'restore'])->name('categories.restore');
    Route::delete('/categories/{id}/delete', [CategoryController::class, 'delete'])->name('categories.delete');
    Route::get('/categories/pdf', [CategoryController::class, 'downloadPdf'])->name('categories.pdf');
    Route::resource('categories', CategoryController::class);

    Route::get('/posts-trash', [PostController::class, 'trash'])->name('posts.trash');
    Route::get('/posts/{id}/restore', [PostController::class, 'restore'])->name('posts.restore');
    Route::delete('/posts/{id}/delete', [PostController::class, 'delete'])->name('posts.delete');
    Route::get('/posts/pdf', [PostController::class, 'downloadPdf'])->name('posts.pdf');
    Route::resource('posts', PostController::class);

    Route::prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('users.index');
        Route::get('/{user}', [UserController::class, 'show'])->name('users.show');
        Route::get('/{user}/change-role', [UserController::class, 'changeRole'])->name('users.change_role');
        Route::patch('/{user}/change-role', [UserController::class, 'updateRole'])->name('users.update_role');
    });
     
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
