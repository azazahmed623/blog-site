<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Backend\Category;

class FrontendController extends Controller
{
    public function index()
    {
        if ($keyword = request('keyword')) {
            $posts = Post::latest()
                        ->where('title', 'LIKE', "%{$keyword}%")
                        ->paginate(15);
        } else {
            $posts = Post::latest()->paginate(15);
        }

        return view('frontend.index', compact('posts'));
    }

    public function postList(Category $category){
        return view('frontend.category', compact('category'));
    }
     
    public function postDetails (Post $post, Category $category){
        return view('frontend.single', compact('post', 'category'));
    }
}
