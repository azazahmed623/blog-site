<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Models\Post;
use Illuminate\Http\Request;
use Image;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Database\QueryException;
use App\Models\Category;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('backend.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('title', 'id')->toArray();
        return view('backend.posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $requestData = [
            'title' => $request->title,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'is_active' => $request->is_active ? true : false,
            'image' => $this->uploadImage($request->file('image'))
        ];

        Post::create($requestData);

        return redirect()
            ->route('posts.index')
            ->withMessage('Successfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Backend\Post  $Post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post, Category $category)
    {
        // $post = Post::find($id);
        return view('backend.posts.show', compact('post', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Backend\Post  $Post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::pluck('title', 'id')->toArray();
        return view('backend.posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Backend\Post  $Post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $Post)
    {
        $requestData = [
            'title' => $request->title,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'is_active' => $request->is_active ? true : false,
            'image' => $this->uploadImage($request->file('image'))
        ];

        if ($request->hasFile('image')) {
            $requestData['image'] = $this->uploadImage($request->file('image'));
        }

        $Post->update($requestData);

        return redirect()
            ->route('posts.index')
            ->withMessage('Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Backend\Post  $Post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $Post)
    {
        $Post->delete();

        return redirect()
            ->route('posts.index')
            ->withMessage('Successfully Deleted');
    }

    public function downloadPdf()
    {
        $posts = Post::all();
        $pdf = Pdf::loadView('backend.posts.pdf', compact('posts'));
        return $pdf->download('Post-list.pdf');
    }

    public function trash()
    {
        $posts = Post::onlyTrashed()->get();
        return view('backend.posts.trash', compact('posts'));
    }

    public function restore($id)
    {
        $Post = Post::onlyTrashed()->find($id);
        $Post->restore();

        return redirect()
            ->route('posts.trash')
            ->withMessage('Successfully restored');
    }

    public function delete($id)
    {
        try {
            $Post = Post::onlyTrashed()->find($id);
            $Post->forceDelete();

            return redirect()
                ->route('posts.trash')
                ->withMessage('Successfully deleted');
        } catch (QueryException $e) {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function uploadImage($image)
    {
        $originalName = $image->getClientOriginalName();
        $fileName = date('Y-m-d') . time() . $originalName;

        // $image->move(storage_path('/app/public/posts'), $fileName); 

        Image::make($image)
            ->resize(200, 200)
            ->save(storage_path() . '/app/public/posts/' . $fileName);

        return $fileName;
    }
}
