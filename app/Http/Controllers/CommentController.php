<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Notifications\NewComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store( Request $request, Post $post){
        $post->comments()->create([
            'body'=> $request->body,
            'Commented_by' => Auth::id()
        ]);

        $user=User::where('email','azazahmed623@gmail.com')->first();
        Notification::send($user, new NewComment($post));
        return redirect()->back();
        

    }
}
