<x-backend.master>
    <!-- Container Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">User Change</h6>
                <a href="{{ route('users.index') }}">User List</a>
            </div>

            <form action="{{ route('users.update_role', $user->id) }}" method="Post" enctype="multipart/form-data">
                @csrf
                @method('patch')

                <div class="form-floating mb-3">
                    <x-forms.select class="form-select" name="role_id" :selected="old('role_id', $user->role_id)" label="Role" :options="$roles" />
                </div>
                <button type="submit" class="btn btn-primary px-5">Update</button>

            </form>
        </div>
    </div>
    <!-- Container End -->
   
</x-backend.master>
