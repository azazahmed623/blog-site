<x-backend.master>
    <!-- Container Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary text-center rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">User List</h6>
                <x-forms.message />
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group me-2">
                        <div><a class="p-2" href="#">PDF </a></div>
                        <div><a class="p-2" href="#">Excel</a></div>
                        <div><a class="p-2" href="#">Trash</a></div>
                        <div><a class="p-2" href="#"><i class="fa fa-plus"
                                    aria-hidden="true"></i> Add New</a></div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table text-start align-middle table-bordered table-hover mb-0">
                    <thead>
                        <tr class="text-white">
                            <th scope="col"><input class="form-check-input" type="checkbox"></th>
                            <th scope="col">SL#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Role</th>
                            <th scope="col">
                                <center>Action</center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td><input class="form-check-input" type="checkbox"></td>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->role->name }}</td>
                                <td>
                                    <center>
                                        <a class="btn btn-sm btn-info"
                                            href="{{ route('users.show', $user->id) }}">Detail</a>
                                            @can('update-role')
                                        <a class="btn btn-sm btn-warning"
                                            href="{{ route('users.change_role', $user->id) }}">Change Role</a>
                                            @endcan
                                    </center>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Container End -->
</x-backend.master>
