<x-backend.master>
    <!-- Container Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">Category Form</h6>
                <a href="{{ route('categories.index') }}">Category List</a>
            </div>

            <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                
                <div class="form-floating mb-3">
                    <x-forms.input type="text" placeholder="Category Title" name="title" required
                        label="Category Title" />
                </div>
                <div class="form-floating mb-3">
                    <x-forms.input type="text" placeholder="Slug" name="slug" label="Slug" />
                </div>
                {{-- <div class="form-floating mb-3">
                    <select class="form-select" id="floatingSelect" aria-label="Floating label select example">
                        <option selected="">Open this select menu</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                    <label for="floatingSelect">Works with selects</label>
                </div> --}}
                <div class="mb-3">
                    <label for="descriptionInput" class="form-label d-black">Description</label>
                    <x-forms.textarea name="description" height= />
                </div>
                <div class="mb-3">
                    <label for="formFileLg" class="form-label">Uplod Image</label>
                    <x-forms.input type="file" class="form-control-lg bg-dark" id="formFileLg" name="image"
                        required />
                </div>
                <div class="mb-3 form-check">
                    @php
                        $checklist = ['Is Active'];
                    @endphp

                    <x-forms.checkbox id="exampleCheck1" :checklist="$checklist" />
                    {{-- <input type="checkbox" class="form-check-input" id="exampleCheck1"> --}}
                    <label class="form-check-label" for="exampleCheck1">Is Active</label>
                </div>
                <button type="submit" class="btn btn-primary px-5">Add</button>

            </form>
        </div>
    </div>
    <!-- Container End -->
    @push('js')
        <script>
            $('#titleInput').on('input', function() {
                let titleInput = $(this).val()
                let slugInput = titleInput.replaceAll(' ', '-')
                $('#slugInput').val(slugInput.tolowerCase());
            })

            ClassicEditor
                .create(document.querySelector('#descriptionInput'))
                .catch(error => {
                    console.error(error);
                });
        </script>
    @endpush

    @push('style')
        <style>
            .ck-content {
                height: 150px;
                background-color: black !important;
                border-color: black !important;
            }
        </style>
    @endpush
</x-backend.master>
