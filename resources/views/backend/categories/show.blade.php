<x-backend.master>
    <!-- Container Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">Single Category Show</h6>
                <a href="{{ route('categories.index') }}">Category List</a>
            </div>
            <div class="row">
                <div class="col-md-6 float-start">
                    <img width="500px" height="400px" src="{{ asset('storage/categories/'.$category->image) }}" />
                </div>
                <div class="col-md-6 float-start">
                    <h6>Title: {{ $category->title }}</h6>
                    <h6>Slug: {{ $category->title }}</h6>
                    <p>Discreptions: This is Category Discreptions</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Container End -->
</x-backend.master>
