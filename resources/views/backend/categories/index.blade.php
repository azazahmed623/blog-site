<x-backend.master>
    <!-- Container Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary text-center rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">Category List</h6>
                <x-forms.message />
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group me-2">
                        <div><a class="p-2" href="{{ route('categories.pdf') }}">PDF </a></div>
                        <div><a class="p-2" href="#">Excel</a></div>
                        <div><a class="p-2" href="{{ route('categories.trash') }}">Trash</a></div>
                        <div><a class="p-2" href="{{ route('categories.create') }}"><i class="fa fa-plus"
                                    aria-hidden="true"></i> Add New</a></div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table text-start align-middle table-bordered table-hover mb-0">
                    <thead>
                        <tr class="text-white">
                            <th scope="col"><input class="form-check-input" type="checkbox"></th>
                            <th scope="col">SL#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Slug</th>
                            <th scope="col">Images</th>
                            <th scope="col">Discreptions</th>
                            <th scope="col">
                                <center>Action</center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                            <tr>
                                <td><input class="form-check-input" type="checkbox"></td>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $category->title }}</td>
                                <td>{{ $category->title }}</td>
                                <td><img width="80px" height="50px"
                                        src="{{ asset('storage/categories/' . $category->image) }}" />
                                </td>
                                <td>This is Category Discreptions</td>
                                <td>
                                    <center>
                                        <a class="btn btn-sm btn-info"
                                            href="{{ route('categories.show', $category->id) }}">Detail</a>
                                        <a class="btn btn-sm btn-warning"
                                            href="{{ route('categories.edit', $category->id) }}">Edit</a>
                                        <form action="{{ route('categories.destroy', $category->id) }}" method="post"
                                            style="display:inline">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-sm btn-danger"
                                                onclick="return confirm('Are you sure want to delete')">Delete</button>
                                        </form>
                                    </center>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Container End -->
</x-backend.master>
