 <div class="container-fluid pt-4 px-4">
     <div class="bg-secondary text-center rounded p-4">
         <div class="table-responsive">
             <table class="table text-start align-middle table-bordered table-hover mb-0">
                 <thead>
                     <tr class="text-white">
                         <th scope="col"><input class="form-check-input" type="checkbox"></th>
                         <th scope="col">SL#</th>
                         <th scope="col">Title</th>
                         <th scope="col">Slug</th>
                         <th scope="col">Images</th>
                         <th scope="col">Discreptions</th>
                     </tr>
                 </thead>
                 <tbody>
                     @foreach ($categories as $category)
                         <tr>
                             <td><input class="form-check-input" type="checkbox"></td>
                             <td>{{ $loop->iteration }}</td>
                             <td>{{ $category->title }}</td>
                             <td>{{ $category->title }}</td>
                             <td><img width="80px" height="50px"
                                     src="{{ asset('storage/categories/' . $category->image) }}" />
                             </td>
                             <td>This is Category Discreptions</td>
                         </tr>
                     @endforeach
                 </tbody>
             </table>
         </div>
     </div>
 </div>
