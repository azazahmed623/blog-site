<x-backend.master>
    <!-- Container Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary text-center rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">Trash Delete List</h6>
                <x-forms.message />
                <a href="{{ route('posts.index') }}">Post List</a>
            </div>
            <div class="table-responsive">
                <table class="table text-start align-middle table-bordered table-hover mb-0">
                    <thead>
                        <tr class="text-white">
                            <th scope="col"><input class="form-check-input" type="checkbox"></th>
                            <th scope="col">SL#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Slug</th>
                            <th scope="col">Images</th>
                            <th scope="col">Discreptions</th>
                            <th scope="col">
                                <center>Action</center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($posts as $post)
                            <tr>
                                <td><input class="form-check-input" type="checkbox"></td>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->title }}</td>
                                <td><img width="80px" height="50px"
                                        src="{{ asset('storage/posts/' . $post->image) }}" />
                                </td>
                                <td>This is post Discreptions</td>
                                <td>
                                    <center>
                                        <a class="btn btn-sm btn-info"
                                            href="{{ route('posts.show', $post->id) }}">Detail</a>
                                        <a class="btn btn-sm btn-warning"
                                            href="{{ route('posts.restore', $post->id) }}">Restore</a>
                                            <form action="{{ route('posts.delete', $post->id) }}" method="post" style="display:inline">
                                                @csrf
                                                @method('delete')
                                                <button class="btn btn-sm btn-danger" onclick="return confirm('Are you sure want to delete')" title="Permanent Delete">Delete</button>
                                            </form>
                                    </center>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Container End -->
</x-backend.master>
