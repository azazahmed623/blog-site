<x-backend.master>
    <!-- Container Start -->
    <div class="container-fluid pt-4 px-4">
        <div class="bg-secondary rounded p-4">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h6 class="mb-0">Single Post Show</h6>
                <a href="{{ route('posts.index') }}">Post List</a>
            </div>
            <div class="row">
                <div class="col-md-6 float-start">
                    <img width="500px" height="400px" src="{{ asset('storage/posts/'.$post->image) }}" />
                </div>
                <div class="col-md-6 float-start">
                    <h6>Title: {{ $post->title }}</h6>
                    <h6>Slug: {{ $post->title }}</h6>
                    <h6>Category: {{ $post->category->title }}</h6>
                    <p>Discreptions: This is post Discreptions</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Container End -->
</x-backend.master>
