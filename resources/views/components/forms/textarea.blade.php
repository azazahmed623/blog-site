@props(['name', 'label' => '', 'value' => null])


    <textarea name="{{ $name }}" id="{{ $name }}Input" {{ $attributes->merge(['class' => 'form-control']) }}>
        {{ $value }}
    </textarea >

    @if($label)
        <label for="{{$name}}Input">{{ $label }}</label>
    @endif

    @error($name)
        <div class="text-danger">{{ $message }}</div>
    @enderror