@props(['name'=> '', 'checklist' => [], 'checkedItems' => [], 'label' => null, 'id'=> null])

@if($label)
    <label class="form-check-label">{{ $label }}</label>
@endif


@foreach($checklist as $key=>$value)

    <input 
        name="{{ $name }}" 
        type="checkbox" 
        id="{{ $key.$id }}Input" 
        value="{{ $key }}"
        @if(in_array($key, $checkedItems))
            checked
        @endif
        {{ $attributes->merge(['class' => 'form-check-input']) }}
    >

    {{-- <label class="form-check-label" for="{{ $key.$id }}Input">{{ $value }}</label> --}}

@endforeach

{{-- @if(in_array($key, $checkedItems))  --}}