@props(['name', 'type', 'label' => '', 'value' => ''])


    <input 
        name="{{ $name }}" 
        type="{{ $type }}"  
        id="{{ $name }}Input"
        value="{{ $value }}"
        {{ $attributes->merge(['class' => 'form-control']) }},
    >

    @if($label)
        <label for="{{$name}}Input">{{ $label }}</label>
    @endif

    @error($name)
        <div class="text-danger">{{ $message }}</div>
    @enderror